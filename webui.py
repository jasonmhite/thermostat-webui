#!/usr/bin/python3
import pandas as pd
import json
import paho.mqtt.client as mqtt

from flask import Flask, render_template, request, send_from_directory
from influxdb import InfluxDBClient, DataFrameClient

app = Flask(__name__)
# app.debug = True

client = mqtt.Client()
def on_connect(cl, udata, flags, rc):
    print("Connected")
    cl.subscribe("controllers/therm0/info")

system_state = {
    "mode": "u",
    "state": "u",
    "set_point": 0,
    "last_temp": 0,
    "allow_auto": 0,
    "override": 0,
}

def on_msg(client, udata, msg):
    mode, state, set_point, last_temp, allow_auto, override  = msg.payload.decode().split()

    print("Received: {}".format(msg.payload))

    system_state["mode"] = mode
    system_state["state"] = state
    system_state["set_point"] = set_point
    system_state["last_temp"] = last_temp
    system_state["allow_auto"] = allow_auto
    system_state["override"] = override

client.on_connect = on_connect
client.on_message = on_msg
client.connect("raspi", 1883, 60)

DB_SRV = {
    "host": "10.1.1.207",
    "username": "root",
    "password": "root",
    "database": "sensors",
}

icli = DataFrameClient(**DB_SRV)

def strtimr(d):
    return d.strftime("%H:%M")

@app.route("/")
def index():
    with open("static/index.html") as f:
        ind = f.read()
    return ind

# Tick date format
# https://github.com/plotly/plotly.js/issues/99

@app.route("/info")
def getInfo():
    return json.dumps(system_state)

@app.route("/data/temps")
def getTemps():
    # q = pd.
    q = icli.query("select * from temperature where time > now() - 1h")["temperature"]

    thermdata = []
    for (label, data) in q.groupby("id"):
        thermdata.append(
            {
                "x": list(map(strtimr, data.index)),
                "y": list(data["value"]),
                "type": "scatter",
                "mode": "lines",
                "name": label
            }
        )

    # thermdata = {
        # "x": list(map(strtimr, q.index)),
        # "y": list(q["value"]),
        # "type": "scatter"
    # }
    return(json.dumps(thermdata))
    # print(thermdata)

@app.route("/data/recent_temps")
def getRecentTemps():
    # q = pd.
    q = icli.query("select * from temperature where time > now() - 5m")["temperature"]

    thermdata = []
    for (label, data) in q.groupby("id"):
        thermdata.append(
            {
                "x": list(map(strtimr, data.index)),
                "y": list(data["value"]),
                "type": "scatter",
                "mode": "lines",
                "name": label
            }
        )

    # thermdata = {
        # "x": list(map(strtimr, q.index)),
        # "y": list(q["value"]),
        # "type": "scatter"
    # }
    return(json.dumps(thermdata))
    # print(thermdata)

@app.route("/control", methods=["GET"])
def setSetPoint():
    params = {
        "mode": None,
        "set_point": None,
        "override": None,
        "allow_auto": None,
    }

    params["set_point"] = request.args.get("set_point", None)
    params["mode"] = request.args.get("mode", None)
    params["override"] = request.args.get("override", None)
    params["allow_auto"] = request.args.get("allow_auto", None)

    print(params)

    if params["set_point"] is not None:
        client.publish("controllers/therm0/control/set_point", params["set_point"])

    if params["mode"] is not None:
        client.publish("controllers/therm0/control/mode", params["mode"])

    if params["override"] is not None:
        client.publish("controllers/therm0/control/override", params["override"])

    if params["allow_auto"] is not None:
        client.publish("controllers/therm0/control/auto", params["allow_auto"])

    return ""

@app.route('/static/<path:path>')
def send_resources(path):
    return send_from_directory('static', path)


if __name__ == "__main__":
    # q = icli.query("select * from temperature where time > now - 3h")
    # print(q)
    client.loop_start()
    app.run(host='0.0.0.0', port=80)
    # app.run()
